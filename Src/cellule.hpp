#ifndef LIFAP6_LISTE_CELLULE_HPP
#define LIFAP6_LISTE_CELLULE_HPP

class Cellule {

  /* votre code ici */
  public:
    Cellule(int _val);
    Cellule(const Cellule& autre);
    ~Cellule();
    Cellule& operator=(const Cellule& autre);


    int valeur;
    Cellule* next;
} ;

#endif
