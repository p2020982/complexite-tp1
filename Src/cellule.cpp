#include "cellule.hpp"

Cellule::Cellule(int _val) {
    next = nullptr;
    valeur = _val;
}

Cellule::Cellule(const Cellule& autre) {
    valeur = autre.valeur;
    next = autre.next;
}

Cellule::~Cellule() {
    delete next;
}

Cellule& Cellule::operator=(const Cellule& autre) {
    valeur = autre.valeur;
    next = autre.next;
}

