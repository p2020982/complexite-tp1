#include "liste.hpp"

#include <iostream>
#include <cassert>

Liste::Liste() {
  cellule_tete = nullptr;
  nb_elem = 0;
}

Liste::Liste(const Liste& autre) {
  cellule_tete = nullptr;
  nb_elem = 0;
  Cellule * cur = autre.cellule_tete;
  Cellule * tete = new Cellule(0);
  Cellule * tmp = tete;
  while (cur!=nullptr) {
    Cellule * cel = new Cellule(cur->valeur);
    tete->next = cel;
    tete = cel;
    cur = cur->next;
    nb_elem++;
  }
  cellule_tete = tmp->next;
  delete tmp;
}

Liste& Liste::operator=(const Liste& autre) {
  Cellule * cur = cellule_tete;
  while(cur != nullptr) {
    Cellule* next = cur->next;
    delete cur;
    cur = next;
  }
  nb_elem = 0;
  cellule_tete = nullptr;
  delete cur;
  cur = autre.cellule_tete;
  Cellule * tete = new Cellule(0);
  Cellule * tmp = tete;
  while (cur!=nullptr) {
    Cellule * cel = new Cellule(cur->valeur);
    tete->next = cel;
    tete = cel;
    cur = cur->next;
    nb_elem++;
  }
  cellule_tete = tmp->next;
  delete tmp;
  return *this;
}

Liste::~Liste() {
  Cellule * cur = cellule_tete;
  while(cur != nullptr) {
    Cellule* next = cur->next;
    delete cur;
    cur = next;
  }
  nb_elem = 0;
  cellule_tete = nullptr;
}

void Liste::ajouter_en_tete(int valeur) {
  Cellule* cel = new Cellule(valeur);
  cel->next = cellule_tete;
  cellule_tete = cel;
  nb_elem++;
}

void Liste::ajouter_en_queue(int valeur) {
  Cellule * cur = cellule_tete;
  Cellule * cel = new Cellule(valeur);
  if(cur == nullptr) {
    cellule_tete = cel;
  } else {
    while (cur->next != nullptr) {
      cur = cur->next;
    }
    cur->next = cel;
  }
  nb_elem++;
}

void Liste::supprimer_en_tete() {
  Cellule* tmp = cellule_tete;
  cellule_tete = cellule_tete->next;
  nb_elem--;
  delete(tmp);
}

Cellule* Liste::tete() {
  /* votre code ici */
  return cellule_tete ;
}

const Cellule* Liste::tete() const {
  /* votre code ici */
  return cellule_tete ;
}

Cellule* Liste::queue() {
  Cellule* cur = cellule_tete;
  for(int i = 1; i<nb_elem; i++) {
    cur = cur->next;
  }  
  return cur;
}

const Cellule* Liste::queue() const {
  Cellule* cur = cellule_tete;
  for(int i = 1; i<nb_elem; i++) {
    cur = cur->next;
  }  
  return cur;
}

int Liste::taille() const {
  /* votre code ici */
  return nb_elem ;
}

Cellule* Liste::recherche(int valeur) {
  Cellule* cur = cellule_tete;
  while(cur != nullptr && cur->valeur != valeur) {
    cur = cur->next;
  }  
  return cur;
}

const Cellule* Liste::recherche(int valeur) const {
  Cellule* cur = cellule_tete;
  while(cur != nullptr && cur->valeur != valeur) {
    cur = cur->next;
  }  
  return cur;
}

void Liste::afficher() const {

  std::cout << "[ ";
  int curval;
  Cellule* cur = cellule_tete;
  for(int i = 0; i<nb_elem; i++) {
    curval = cur->valeur;
    std::cout << curval << " ";
    cur = cur->next;
  }
  std::cout << "]" << std::endl;
}
